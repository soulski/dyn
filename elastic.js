const elastic = require('elasticsearch')

const buildDb = (config) => {
    const cli = new elastic.Client({ 
        host: config.connection.endpoint 
    });

    const exists = (indices) => {
        const tasks = indices.map( indice => {
            return cli.indices.exists({
                index: indice.name 
            }).then(data => {
                indice.exists = data;
            });
            
        })

        return Promise.all(tasks).then(() => {
            return indices;    
        })
    }

    const remove = (indices) => {
        const removeIndices = indices.filter( indice => indice.exists )
        const tasks = removeIndices.map( indice => {
            return cli.indices.delete({
                index: indice.name
            });
        });

        return Promise.all(tasks).then(() => indices);
    }

    const create = (indices) => {
        const tasks = indices.map( indice => {
            return cli.indices.create({
                index: indice.name,
                mapping: indice.mapping
            }).then(() => {
                console.log(`Indice ${indice.name} has been created`);    
                return Promise.resolve(...arguments);
            });
        });
        
        return Promise.all(tasks);
    }


    return exists(config.indices)
        .then(remove)
        .then(create);
}

module.exports = buildDb;
