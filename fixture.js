const aws = require('aws-sdk'),
      prettyjson = require('prettyjson'),
      _ = require('lodash');


const insert = (db) => (table, data) => {
    const param = {
        TableName: table,
        Item: data,
        ReturnConsumedCapacity: 'NONE'
    }

    return new Promise((resolve, reject) => {
        db.put(param, (err, data) => {
            if (err) 
                return reject(err);

            resolve(data);
        }); 
        
    })
}

const fixture = (connection, fixtures) => {
    const db = new aws.DynamoDB.DocumentClient(connection),
          put = insert(db);

    _.forIn(fixtures, (rows, table) => {
        const tasks = rows.map(row => {
            return put(table, row);    
        })

        Promise.all(tasks).then(() => {
            console.log(`Create fixture on table ${table} is done.`);    
        }).catch(err => {
            console.log(err);    
        })
    });
};

module.exports = fixture;
