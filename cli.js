#!/usr/bin/env node

const program = require('commander'),
      fs = require('fs'),
      dyn = require('./index.js');

program
    .version('0.1.0')
    .command('build [config file]')
    .alias('b')
    .description('build database from scratch from config file')
    .action(configPath => {
        const config = JSON.parse(fs.readFileSync(configPath));
        dyn.build(config);
    });

program
    .command('list [config file]')
    .alias('l')
    .option('-v --verbose', 'Show table detail')
    .description('list all table')
    .action((configPath, options) => {
        const config = JSON.parse(fs.readFileSync(configPath));
        dyn.list(config.connection, options.verbose);
    });

program
    .command('fixture [config file]')
    .alias('f')
    .description('insert all fixture')
    .action((configPath, options) => {
        const config = JSON.parse(fs.readFileSync(configPath));
        dyn.fixture(config.connection, config.fixtures);
    });

program.parse(process.argv);
