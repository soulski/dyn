const aws = require('aws-sdk'),
      prettyjson = require('prettyjson'),
      _ = require('lodash');

const list = (config, verbose) => {
    const db = new aws.DynamoDB(config);

    const listTable = () => {
        return new Promise((resolve, reject) => {
            db.listTables({}, (err, data) => {
                if (err)
                    return reject(err)

                resolve(data.TableNames);
            })
        });
    }

    const describeTable = (table) => {
        return new Promise((resolve, reject) => {
            db.describeTable({ TableName: table }, (err, data) => {
                if (err) 
                    return reject(err)

                resolve(data.Table);
            });
        })
    }

    return listTable()
        .then(tables => {
            if (verbose) {
                const tasks = tables.map(describeTable);

                return Promise.all(tasks)
                    .then(tables => {
                        tables = tables.map(table => {
                            return _.pick(table, [
                                'TableName',
                                'AttributeDefinitions',
                                'KeySchema',
                                'ItemCount',
                                'StreamSpecification'
                            ]);
                        });

                        return Promise.resolve(tables);
                    });
                
            }

            return tables;
        })
        .then(tables => {
            console.log(prettyjson.render(tables));  
            return tables;
        })
        .catch(err => {
            console.err(err);    
        });
}

module.exports = list;
