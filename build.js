const aws = require('aws-sdk'),
      _ = require('lodash');

const TYPE = {
    "string": "S",
    "number": "N",
    "buffer": "B",
    "set_string": "SS",
    "set_number": "NS",
    "set_buffer": "BS",
    "map": "M",
    "list_map": "L",
    "null": "NULL",
    "boolean": "BOOL"
};

const getType = type => TYPE[type];

const KEY_TYPE = {
    "range": "RANGE",
    "hash": "HASH"
};

const getKeyType = type => KEY_TYPE[type];

const STREAM_TYPE = {
    "NewOld": "NEW_AND_OLD_IMAGES",
    "Old": "OLD_IMAGE",
    "New": "NEW_IMAGE",
    "Key": "KEYS_ONLY",
};

const getStreamType = type => STREAM_TYPE[type];

const PROJECTION_TYPE = {
    "all": "ALL",
    "keysOnly": "KEYS_ONLY",
    "include": "INCLUDE"
};

const getProjectionType = type => PROJECTION_TYPE[type];

const buildDb = (config) => {
    const db = new aws.DynamoDB(config.connection);

    const existsTable = tables => {
        const tasks = tables.map(table => {
            return new Promise((resolve, reject) => {
                db.describeTable({ TableName: table }, (err, data) => {
                    if (err) 
                        resolve({ name: table, exists: false });
                    else 
                        resolve({ name: table, exists: true });
                })    
            });
        });

        const pickExistsTable = tables => {
            return tables.filter(table => table.exists)
                        .map(table => table.name)
        }

        return Promise.all(tasks).then(pickExistsTable);
    }

    const deleteTable = tables => {
        const tasks = tables.map(table => {
            return new Promise((resolve, reject) => {
                db.deleteTable({ TableName: table }, (err, data) => {
                    if (err)
                        reject(err);
                    else 
                        resolve(data.TableDescription.TableName);
                });    
            });
        });

        return Promise.all(tasks);
    }

    const createTable = tables => {
        const tasks = tables.map(table => {
            return new Promise((resolve, reject) => {
                let param = {
                    TableName: table.name,
                    AttributeDefinitions: [],
                    KeySchema: [],
                    ProvisionedThroughput: {
                        ReadCapacityUnits: 5,
                        WriteCapacityUnits: 5
                    }
                };

                _.keys(table.attributes).forEach(name => {
                    const desc = table.attributes[name];

                    param.AttributeDefinitions.push({
                        AttributeName: name,
                        AttributeType: getType(desc.type)
                    }); 

                    if (_.has(desc, 'key')) {
                        param.KeySchema.push({
                            AttributeName: name,
                            KeyType: getKeyType(desc.key)
                        }); 
                    }
                })

                if (table.globalIndexs) {
                    param.GlobalSecondaryIndexes = table.globalIndexs.map(index => {
                        const { name, keys, projection } = index;
                        const throughput = index.throughput || { read: 5, write: 5 };

                        const getProjection = (projection) => {
                            const type = getProjectionType(projection.type) || 'KEYS_ONLY';
                            const config = {
                                ProjectionType: type
                            };

                            if ('INCLUDE' === type) {
                                config.NonKeyAttributes = projection.attributes;
                            }

                            return config;
                        }

                        return {
                            IndexName: name,
                            KeySchema: keys.map(key => ({
                                AttributeName: key.name,
                                KeyType: getKeyType(key.type)
                            })), 
                            Projection: getProjection(projection),
                            ProvisionedThroughput: {
                                ReadCapacityUnits: throughput.read,
                                WriteCapacityUnits: throughput.write,
                            },
                        }; 
                    });
                }

                if (table.stream) {
                    param.StreamSpecification = {
                        StreamEnabled: true,
                        StreamViewType: getStreamType(table.stream)
                    }
                }


                db.createTable(param, (err, data) => {
                    if (err)
                        reject(err);
                    else
                        resolve(data.TableDescription.TableName);
                })    
            });
        });

        return Promise.all(tasks);
    }

    const tableNames = config.tables.map(table => table.name)
    const deleteTables = config.deleteTables;

    return Promise.resolve(tableNames) 
        .then(existsTable)
        .then(deleteTable)
        .then(tables => { 
            tables.forEach(
                table => console.log(`Table ${table} has been deleted`)
            );    
        })
        .then(() => {
            return config.tables;    
        })
        .then(createTable)
        .then(tables => {
            tables.forEach(
                table => console.log(`Table ${table} has been created`)
            );    

            return tables;
        })
        .catch(e => {
            console.log(e);    
        });
}

module.exports = buildDb;
